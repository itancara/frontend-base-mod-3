import Vue from 'vue'
import Titulo from 'components/common/Titulo'
import Estado from 'components/common/Estado'

Vue.component('Estado', Estado)
Vue.component('Titulo', Titulo)
