export default ({ Vue }) => {
  Vue.prototype.$storage = {
    get (keyName) {
      if (localStorage.getItem(keyName)) {
        try {
          return JSON.parse(localStorage.getItem(keyName))
        } catch (error) {
          return localStorage.getItem(keyName)
        }
      }
    },
    set (keyName, value) {
      localStorage.setItem(keyName, JSON.stringify(value))
    },
    remove (keyName) {
      localStorage.removeItem(keyName)
    },
    clear () {
      localStorage.clear()
    }
  }
}
