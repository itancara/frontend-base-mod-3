import { Notify } from 'quasar'

export default async ({ Vue }) => {
  Vue.prototype.$alert = {
    success (message) {
      Notify.create({
        color: 'white',
        message,
        icon: 'done_all',
        position: 'top-right',
        textColor: 'positive',
        actions: [
          {
            textColor: 'positive',
            icon: 'close'
          }
        ]
      })
    },
    error (message) {
      Notify.create({
        color: 'white',
        message,
        icon: 'report_problem',
        position: 'top-right',
        textColor: 'negative',
        actions: [
          {
            textColor: 'negative',
            icon: 'close'
          }
        ]
      })
    }
  }
}
