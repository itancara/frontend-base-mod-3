import axios from 'axios'
const protocol = window.location.protocol.replace(':', '')
const PATERN_HOST = protocol === 'https' ? /(https:\/\/|www\.)\S+/i : /(http:\/\/|www\.)\S+/i

export default ({ Vue, store, router }) => {
  const Services = {
    get (url, id, loading = true) {
      return _http('GET', url, id, loading)
    },
    post (url, data, loading = true) {
      return _http('POST', url, data, loading)
    },
    put (url, data, loading = true) {
      return _http('PUT', url, data, loading)
    },
    patch (url, data, loading = true) {
      return _http('PATCH', url, data, loading)
    },
    delete (url, data, loading = true) {
      return _http('DELETE', url, data, loading)
    },
    list (url, query, loading = true) {
      let string = []
      for (const key in query) {
        string.push(`${key}=${query[key]}`)
      }
      string = string.join('&')

      string = url.includes('?') ? `&${string}` : string = `?${string}`
      return _http('GET', `${url}${string}`, {}, loading)
    }
  }

  function getUrl (url, data) {
    let id = ''
    if (typeof data === 'object') {
      id = data.id
    }

    if (typeof data === 'string' || typeof data === 'number') {
      id = data
    }

    id = id ? id.toString() : ''
    if (url[url.length - 1] !== '/' && id.length > 0) {
      id = `/${id}`
    }
    return PATERN_HOST.test(url) ? `${url}${id}` : `${process.env.BACKEND_URL}${url}${id}`
  }

  async function _http (method, url, data, loading) {
    try {
      if (loading) {
        Vue.prototype.$q.loading.show({ message: 'Cargando...' })
      }
      url = getUrl(url, data)

      const setting = {
        method,
        url
      }

      if (typeof data === 'object' && Object.keys(data).length) {
        delete data.id
        setting.data = data
      }

      setting.headers = {}

      const respuesta = await axios(setting)
      return respuesta.data.datos
    } catch (error) {

    } finally {
      if (loading) {
        Vue.prototype.$q.loading.hide()
      }
    }
  }

  Vue.prototype.$axios = Services
}
